DESCRIPTION = "A smart console-only image with more full-featured Linux system \
functionality installed."

IMAGE_FEATURES += "splash package-management ssh-server-openssh"

inherit core-image
